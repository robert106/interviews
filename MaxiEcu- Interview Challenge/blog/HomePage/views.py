from django.shortcuts import render
from .models import Post
from .form import PostForm
from django.views.generic import DetailView
from django.db.models import Q

def index(request):
  posts = Post.objects.all().order_by('-created_at')
  
  # searching logic
  query = request.GET.get("q")
  if query:
    queryset_list = posts.filter(
      Q(author__icontains=query) |
      Q(place__icontains=query)
      )

    return render(request, 'index.html', {"list":queryset_list})


  return render(request, 'index.html', {'posts':posts})


def add_post(request):
  if request.method == 'POST':
    form = PostForm(data=request.POST,files=request.FILES)
    if form.is_valid():
      form.save()
      obj=form.instance
      
      return render(request,"add-post.html",{"obj":obj})
  else:
    form = PostForm()
    
  return render(request,"add-post.html",{"form":form})


class ArticleDetailView(DetailView):
  model = Post
  template_name = 'blog-single.html'


def newsletter(request):
  return render(request,'newsletter.html')