from django.urls import path
from . import views
from .views import ArticleDetailView

urlpatterns = [
 path('',views.index,name='home-page'),
 path('post/<int:pk>', ArticleDetailView.as_view(),name='post-detail'),
 path('add-post/',views.add_post,name='add-post'),
] 
