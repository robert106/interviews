from django.db import models
# Create your models here.

class Post(models.Model):
 title = models.CharField(max_length=200)
 image = models.ImageField(upload_to = 'pics')
 description = models.TextField()
 author = models.CharField(default='author',max_length=100)
 created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
 updated_at = models.DateTimeField(auto_now=True, auto_now_add=False)
 place = models.CharField(max_length=100,null=True)

 def __str__(self):
  return self.title
 
 def snippet(self):
  return self.description[:40] +'...'

