from django.shortcuts import redirect, render
from . forms import MailMessageForm
from django.core.mail import send_mail
# Create your views here.

def contact(request):
  if request.method == 'POST':
    form = MailMessageForm(request.POST)
    if form.is_valid():
      form.save()
      title = form.cleaned_data.get('title')
      message = form.cleaned_data.get('message')
    send_mail( 
    title,
    message,
    '',
    # to jest mail od ktorego wlasciciel dostaje maila, ustwiony w settings.py
    ['robertaugustyniak91@gmail.com'], 
  #  To ^ jest mail wlasciciela bloga 
    fail_silently=False 
  )
    return redirect('/')
  else:
    form = MailMessageForm()
  context = {
    'form':form,
  }
  return render(request, 'contact.html', context)