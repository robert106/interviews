from django import forms
from . models import MailMessage


class MailMessageForm(forms.ModelForm):
 class Meta:
  model = MailMessage
  fields = '__all__'