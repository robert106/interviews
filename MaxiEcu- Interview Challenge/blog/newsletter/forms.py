from django import forms
from . models import Subscribers, SendNewsletterMessage

class SubscribersForm(forms.ModelForm):
 class Meta:
  model = Subscribers
  fields = ['email']


class SendNewsletterForm(forms.ModelForm):
 class Meta:
  model = SendNewsletterMessage
  fields = '__all__'