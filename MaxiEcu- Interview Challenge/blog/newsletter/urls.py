from django.urls import path
from . import views

urlpatterns = [
 path('', views.newsletter,name='newsletter'),
 path('newsletter-message', views.send_newsletter_message,name='newsletter-message'),
]
