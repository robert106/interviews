from django.db import models

# Create your models here.

class Subscribers(models.Model):
  email = models.EmailField(null=True,max_length=200)
  sign_in_date = models.DateTimeField(auto_now_add=True,auto_now=False)

  def __str__(self):
    return self.email


class SendNewsletterMessage(models.Model): 
  title = models.CharField(max_length=150, null=True)
  message = models.TextField(null=True)

  def __str__(self):
    return self.title

