from django.contrib import admin
from . models import Subscribers, SendNewsletterMessage
# Register your models here.

admin.site.register(Subscribers)
admin.site.register(SendNewsletterMessage)
