from django.shortcuts import render, redirect
from . forms import SubscribersForm, SendNewsletterForm
from django.core.mail import send_mail
from django_pandas.io import read_frame
from . models import Subscribers
# Create your views here.

def newsletter(request):
  if request.method == 'POST':
    form = SubscribersForm(request.POST)
    if form.is_valid():
      form.save()

      return redirect('/')
  else:
    form = SubscribersForm()
  context = {
    'form':form,
}

  return render(request,'newsletter.html',context)


def send_newsletter_message(request):
  emails = Subscribers.objects.all()
  data_frame = read_frame(emails, fieldnames=['email'])
  mail_list = data_frame['email'].values.tolist()
  if request.method == 'POST':
    form = SendNewsletterForm(request.POST)
    if form.is_valid():
      form.save()
      title = form.cleaned_data.get('title')
      message = form.cleaned_data.get('message')
      send_mail(
        title,
        message,
        '',
        mail_list,
        fail_silently=False,
)
      return redirect('/')
  else:
    form = SendNewsletterForm()
  context = {
    'form':form,
  }
  
  return render(request, 'newsletter-message.html', context)