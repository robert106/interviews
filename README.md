### Aby uruchomić serwer nalezy:
- Stworzyć i aktywować wirtualne srodowisko poprzez następujące komendy (macOS):
    - python3 -m venv .venv
    - source .venv/bin/activate
    
- Następnie wpisać następującą komendę:
    - pip install -r requirements.txt

- Uruchomić server komendą:
    - python3 manage.py runserver 
    (Jezeli pojawi się błąd mówiący o tym, ze server jest zajęty nalezy do powyzszej komendy dopisac port na jakim serwer ma pracować np. python3 manage.py runserver 9000)

Login i Hasło admina(superuser):
login : admin
hasło : admin12345

### Zadania:
- [x] REST API do zarządzania wpisami
- [x] System logowania
- [x] Moliwość dodania wpisu z wymaganym zdjęciem, opisem, miejscem oraz podpisem autora 
      + automatyczne nadawanie daty i godziny wrzucenia wpisu
- [x] Moliwość zedytowania istniejących juz wpisów, zanotowanie daty edycji (tylko admin moze to zrobic)
- [x] Moliwość usunięcia wpisów (tylko admin)
- [x] Lista maili zapisanych do newslettera

- [x] REST API do przeglądania wpisów (zdjęcie, opis, tytuł, miejsce, podpis autora, data dodania wpisu, data edycji )
- [x] Ustawiłem automatyczne filtrowanie po dacie dodania (Posty na blogu pojawiają się od najnowszego do   najstarszego)
- [x] Wyszukiwanie po autorach oraz miejscu
- [x] Mośliwość zapisania się do newslettera
- [x] Dodanie zakładki kontakt, w której mozna zadac pytanie
- [x] Po zadaniu pytania w zakładce "kontakt" wysyłanie maila z treścią na email właściciela bloga (Właściciel ustawiony jest w letter_messsage -> views.py) 
- [x] Wykorzystanie PostgreSQL jako bazy danych  